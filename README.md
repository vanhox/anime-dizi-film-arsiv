# anime-dizi-film
## * [Anime](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#anime)
### > [Streaming](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#streaming-1)
#### -[İngilizce Altyazılı](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#i%CC%87ngilizce-altyaz%C4%B1l%C4%B1)
#### - [Türkçe Altyazılı](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#t%C3%BCrk%C3%A7e-altyaz%C4%B1l%C4%B1)
### > [Download Only](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#download-only)
## * [Manga](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#manga)
#### -[İngilizce](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#i%CC%87ngilizce)
#### -[Türkçe](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#t%C3%BCrk%C3%A7e)
## * [Dizi/Film](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#dizifilm)
### > [Streaming](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#streaming-1)
#### -[İngilizce Altyazılı](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#i%CC%87ngilizce-altyaz%C4%B1l%C4%B1-1)
#### -[Türkçe Altyazılı](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#t%C3%BCrk%C3%A7e-altyaz%C4%B1l%C4%B1-1)
### > [Download Only](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#download-only-1)
## * [Altyazılar](https://gitlab.com/vanhox/anime-dizi-film-arsiv/-/edit/master/README.md#altyaz%C4%B1lar)

## Anime
### Streaming
#### İngilizce Altyazılı
- [Crunchyroll](https://www.crunchyroll.com/) (Semi-Paid) (No-AdBlock Blocker) İngilizce streaming denilince ilk akla gelen sitelerden biri, video ve altyazı kalitesi bir numara diyebilirim. Bunun yanında eksileri de var, premium ve non-premium seçenekleri var sitede. Reklamsız ve en hızlı şekilde ulaşmak istiyorsanız premium üyelik almanız gerekiyor, bazı bölümleri ya da animeleri izleyemeyebiliyorsunuz sizin bölgenizde açık olmadığı için. Bu nedenle free-to-use ama micro-transaction var diyebiliriz sitede.
- [4Anime](https://4anime.to/) (Free) (No-AdBlock Blocker) En bilindik ücretsiz anime streaming sitelerinden biridir, bazen siteye aşırı yüklenme olabiliyor, eğer çalışmıyorsa yazacağım diğer siteleri deneyin.
- [9Anime](https://9anime.to/home) (Free) (No-AdBlock Blocker) Bu da çok bilindik bir sitedir, şu ana kadar hiç aşırı yüklenme yaptığını görmedim yani bu site genellikle açıktır.
- [AniWatch](https://aniwatch.me/home) (Free) (Ad-Free) Reklamsız gayet güzel bir streaming sitesi.
- [AnimeSaga](https://animesa.ga/) (Free) 
- [AnimeSimple](https://ww1.animesimple.com/) (Free) (No-AdBlock Blocker)
- [AnimeTwist](https://twist.moe/) (Free) (No-AdBlock Blocker) Siteye girince bön bön bakmayın, site diğerleri gibi katalog içermiyor, hemen karşınızda search kısmı var. İzlemek istediğiniz animenin adını yazın ve sonuçlardan tıklayıp izleyin.
- [Anime8](https://anime8.ru/) (Free) (No-AdBlock Blocker) Rus sitesi diye ön yargı yapmayın site çok başarılı.
- [AnimeHeaven](https://animeheaven.ru/animeheaven.eu) (Free) (No-AdBlock Blocker) Bu da üstteki gibi.
#### Türkçe Altyazılı
- [TürkAnimeTV](https://www.turkanime.net/) (Free) (No-AdBlock Blocker) Türkiye'deki en iyi ve en güvenilir anime sitesidir. Alternatifleri biraz sıkıntılı olabiliyor çünkü birçoğu kaldırılıyor. Video kalitesinden biraz ödün verip Türkçe izlemek istiyorsanız aradığınız yer burasıdır.
### Download Only
- [AniChiraku](https://anichiraku.ru) (Private) (Ad-Free) Bu private bir site, yılın bazı zamanlarında sitelerine üyelik kabulu açıyorlar. 
- [AniDex](https://anidex.info/) (Public) (Torrent-Only) (Ad-Free) Geniş bir kitlesi olan, hızlıca bölümlerin paylaşıldığı bir site.
- [AniDL](https://anidl.org/) (Public) (Ad-Free)
- [Anime Archive](https://anime-archive.com/) (Public) (Ad-Free) Alman sitesi
- [Nyaa](https://nyaa.si/) (Public) (Ad-Free) İçinde birçok dilde bölüm var, çok geniş bir site.
- [AnimeKaizoku](https://animekaizoku.com/) (Public) (Ad-Free)
- [AnimeTosho](https://animetosho.org/) (Public) (Ad-Free)
### Streaming ve Download Only
- [KawAnime](https://kawanime.com/) (Free) Bu bir anime streaming ve download programı. İstediğiniz bölümü ister seyredip ister indirebilirsiniz. Bu program anilist ve myanimelist ile de bağlantılı, izlediğiniz bölümleri ilerleme olarak kaydedebiliyor ve bu sitelerden haber akışı ve anime açıklamalarını sağlayabiliyorsunuz.
## Manga
#### İngilizce
- [ComicK](https://comick.fun/) (Free)
- [Manga4Life](https://manga4life.com/) (Free)
- [MangaFox](https://fanfox.net/) (Free)
- [MangaHere](https://www.mangahere.cc/) (Free)
- [Manga+](https://mangaplus.shueisha.co.jp/updates) (Free)
- [MangaPark](https://mangapark.net/) (Free)
- [MangakaLot](https://mangakakalot.com/) (Free)
- [MangaFreak](https://w11.mangafreak.net/) (Free)
#### Türkçe
- [MangaDenizi](https://mangadenizi.com/) (Free)
### Mobil Manga Okuyucular
- [Tachiyomi](https://tachiyomi.org/) (Android)
- [MangaxManga](https://mangaxmanga.com/) (IOS)
## Dizi/Film
### Streaming 
#### İngilizce Altyazılı
- [WatchSeries](https://www.watchseries.ninja/) (Free)
- [SolarMovie](https://www.solarmovie.one/) (Free)
- [LookMovie](https://lookmovie.io/) (Free)
- [PrimeWire](https://www.primewire.li/) (Free)
- [NOXX](https://noxx.is/) (Free)
- [WatchEpisodes](https://www.watchepisodes4.com/) (Free)
- [HiMovies](https://www3.himovies.to/) (Free)
- [LosMovies](https://losmovies.live/) (Free)
- [FMovies](https://ffmovies.co/home) (Free)
#### Türkçe Altyazılı
- [Dizigom](https://www.dizigom1.com) (Free) (Ad-Free)
- [Dizimag](https://www.dizimag2.org) (Free)
### Download Only
- [TorrentGalaxy](https://torrentgalaxy.to/) (Public)
- [SolidTorrents](https://solidtorrents.net/movies) (Public)
- [1337x](https://1337x.to/) (Public)
- [Rutracker](https://rutracker.org/forum/index.php) (Public)
## Altyazılar
- [OpenSubtitles](https://www.opensubtitles.org) 
- [SubScene](https://subscene.com/)
- [Addic7d](https://www.addic7ed.com/)
